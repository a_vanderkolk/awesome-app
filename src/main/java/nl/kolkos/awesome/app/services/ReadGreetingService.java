package nl.kolkos.awesome.app.services;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ReadGreetingService {

    @ConfigProperty(name = "PERSONAL_MESSAGE")
    String personalMessage;

    public String getGreeting() {
        return personalMessage;
    }

}

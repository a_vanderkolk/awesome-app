package nl.kolkos.awesome.app;

import nl.kolkos.awesome.app.services.ReadGreetingService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @Inject
    ReadGreetingService readGreetingService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("message")
    public String getPersonalGreeting() {
        return readGreetingService.getGreeting();
    }
}